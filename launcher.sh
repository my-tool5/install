#!/usr/bin/env sh
set -e

## Definimos variables
TMP_DIR=/tmp/my-tool5-installer
OS=null
FPK=0
SNP=0

## Preparamos el directorio de trabajo: /tmp/
mkdir -p $TMP_DIR

git clone --recurse-submodules https://gitlab.com/my-tool5/all-tool5.git $TMP_DIR

## Recuperamos los datos del Sistema:
DIRS=$(for i in $(ls $TMP_DIR); do echo "$i"; done)
DIR_CONTENT=$(for i in $DIRS      ### Outer for loop ###
do
    printf "$TMP_DIR/"$i"\n" 
    for j in $(ls -a $TMP_DIR/"$i") ### Inner for loop ###
    do
          printf "\t$j\n"
    done

  echo "" #### print the new line ###
done
)

cat > "$HOME"/install.log <<- _EOF_
Hemos creado los siguientes directorios:

    $TMP_DIR

Que contine lo siguiente:

$DIR_CONTENT

Con las siguientes variables:
TMP_DIR=$TMP_DIR
OS=$OS
FPK=$FPK
SNP=$SNP

_EOF_
rm -rf $TMP_DIR

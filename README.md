# My-Tool5 Installer

Script para preparar, configurar e instalar el software necesario en cualquier distro Linux.

### Instalación
Lanzamos el script install-all.sh de la siguiente forma.
```
$ curl https://gitlab.com/my-tool5/install/launcher.sh | sh
```
